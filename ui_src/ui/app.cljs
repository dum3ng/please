(ns ui.app
  (:require [ui.panels.core :as p]
            [reagent.core :as r]
            [re-com.core :as rc]
            [ui.components.core :refer [scrollbars]]
            [re-frame.core :refer [subscribe dispatch]]))


(defn flash-msg
  []
  (let [flash (subscribe [:flash])]
    [:div {:style {:position "fixed"
                   :width "100%"
                   :height 30
                   :bottom 0
                   :display (if (:open @flash) "block" "none")
                   :background "black"
                   :color "white"
                   :text-align "center"
                   :font-size 18}}
     [:p (:message @flash)]
     [:b {:on-click #(dispatch [:flash/close])} "X"]]))


(defn test-view
  []
  (let [x (r/atom 3)]
    [:div "number is "
     [:b @x]
     [:button {:on-click #(swap! x inc)} "inc"]]))
(defn app
  []
  [:div.d-flex.flex-row.h-100.w-100 
   [flash-msg]
   [rc/h-split
    :style {:margin 0}
    :height "100%"
    :panel-1 [p/files-panel]
    :panel-2 [scrollbars {:style {:width 200
                                  :height "100%"}}
              [p/search-panel]]
    :size "400px"]
   ;; [p/files-panel]
   ;; [p/search-panel]
   [p/compose-panel]])

;; (defn app
;;   []
;;   [:div
;;    [:h2 "hello"]
;;    [scrollbars {:style {:width 250 :height 100}}
;;     [:div {:style {:background-color "red"
;;                    :height 300}}
;;      "hello"
;;      [b4/card {:style {:width 250}}
;;       [b4/card-image]
;;       [b4/card-body
;;        [b4/card-title "world"]]]]]])
