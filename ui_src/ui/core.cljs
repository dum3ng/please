(ns ui.core
  (:require [reagent.core :as r :refer [atom]]
            [ui.events]
            [ui.subs]
            [re-frame.core :refer [dispatch-sync]]
            [ui.app :refer [app]]
            [ui.db :as db]
            [cljsjs.react.custom.scrollbars]))

(enable-console-print!)

;; we can get external library from deps
;; (def Awesome (aget js/deps "awesome-component"))
;; (def view []
;;   [:> Awesome {:name "me"}])
;; (def electron (js/require "electron"))
;; (def remote (aget electron "remote"))


(defn init!
  []
  (db/init-db!)
  (dispatch-sync [:db/initialize-prod])
  (r/render
   [app]
   (js/document.getElementById "app-container"))  )


(init!)
#_(r/render
    [app]
    (js/document.getElementById "app-container"))  

