(ns ui.panels.files-panel
  (:require [re-frame.core :refer [dispatch subscribe]]
            [ui.db :as db]
            [ui.components.core :as c :refer [divider]]
            [reagent.core :as r]))


(defn tag-label
  [node-id tag-id]
  (let [tag (subscribe [:tag tag-id])
        show (r/atom false)]
    (fn [node-id tag-id]
      [:span.small-tag.mr-2 {:on-mouse-over #(do (print "mouse over")(reset! show true))
              :on-mouse-out #(reset! show false)
}
       [:i.px-1 (:name @tag)]
       [:a.delete-tag [:i.fa.fa-close {:style {:opacity (if @show 1 0)}
                                       :on-click #(dispatch [:file/remove-tag node-id tag-id])}]]])))

(defn node-view
  [node]
  (let [id @(subscribe [:current-node-id])]
    [c/list-group-item {:class (if (= id (:id node)) "active" "")
                     :style {:border-bottom "solid 1px gray"}

                     :on-click #(dispatch [:node/select(:id node)])}
     [:div.d-flex.flex-row.justify-content-between
      [:b (:name node)]
      [:a.align-self-end.delete-node [:i.fa.fa-trash {:on-click #(dispatch [:node/delete (:id node)])}]]]
     [:div.d-flex.flex-row.flex-wrap
      (for [t (:tags node)]
        ^{:key t} [tag-label (:id node) t ])]]) )

(defn files-view
  "show the files structure."
  [nodes current ]
  [c/list-group
   (for [k (keys nodes)]
     ^{:key k}[node-view (get nodes k)])])

(defn files-panel
  []
  (let [nodes (subscribe [:nodes])
        current-node-id (subscribe [:current-node-id])]
    [:div.container-fluid.panel.files-panel
     [:a {:on-click #(dispatch [:db/save])}
      [:i.fa.fa-database] "save db"]
     [divider]
     [:div.d-flex.flex-row-reverse
      [:a {:on-click #(dispatch [:node/add {:name "new added"} ])}
       [:span [:i.fa.fa-plus] ]]] 
     [divider]
     [files-view @nodes @current-node-id]])
  )
