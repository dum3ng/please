(ns ui.panels.compose-panel
  (:require [reagent.core :as r]
            [ui.config :as c]
            [ui.utils :as u]
            [ui.components.core :refer [scrollbars]]
            [re-frame.core :refer [subscribe dispatch]]
            [ui.fake-content :refer [content]]))


(defn event-value
  [e]
  (.-value (.-target e)))


(defn title-view
  [node]
  (let [editing (r/atom false)
        title (r/atom (:name node))
        t (r/atom -1)]
    (r/create-class
     {:component-will-receive-props
      (fn [this argv]
        (let [[_ nnode] argv]
          (reset! title (:name nnode))
          (reset! editing false)))
      :reagent-render
      (fn [{:keys [id name] :as node}]
        [:input.title-input.pl-1 {:on-change (fn [e]
                              (do
                                (reset! title (event-value e))
                                (if @editing
                                  (js/clearTimeout @t))
                                (reset! editing true)
                                (reset! t (js/setTimeout #(do
                                                            (reset! editing false)
                                                            (dispatch [:file/set-title id @title ])) c/title-delay))))
                             :value @title}])}
     )))


(defn content-view
  [node c]
  (let [editing (r/atom false)
        edited (r/atom false)
        content (r/atom c )
        t (r/atom -1)]
    (r/create-class
     {:component-will-receive-props
      (fn [this argv]
        (print "receive in content view..")
        (let [[_ onode oc] (r/argv this)
              [_ nnode nc] argv]
          (when (and (not= (:id onode) (:id nnode))
                   @edited)
            (dispatch [:buffer/save-file (:id onode) @content])
            (js/clearTimeout @t))
          (reset! content nc)
          (reset! editing false)
          (reset! edited false)))
      :reagent-render
      (fn [{:keys [id] :as node} c]
        [:textarea.editor {:on-change (fn [e]
                                        (do
                                          (reset! edited true)
                                          (reset! content (event-value e))
                                          (if @editing
                                            (js/clearTimeout @t))
                                          (reset! editing true)
                                          (reset! t (js/setTimeout #(do
                                                                      (reset! editing false)
                                                                      (dispatch [:buffer/save-file id @content ])) c/content-delay))))
                           :value @content}])})))

;; (defn editor-1
;;   [{:keys [id name] :as node} b]
;;   (let [t (r/atom "")]
;;     (r/create-class
;;      {:component-will-receive-props
;;       (fn [this argv]
;;         (let [[_ onode oc] (r/argv this)
;;               [_ nnode nc] argv]
;;           (print "old node: " (:id onode)
;;                  "new node: " (:id nnode))
;;           (if (and (not= (:id onode) (:id nnode))
;;                    @edited)
;;             (dispatch [:buffer/save-file (:id onode) @v])))
;;         (reset! v (:content (nth argv 2)))
;;         (reset! title (:name (nth argv 1)))
;;         (reset! edited false)
;;         )
;;       :reagent-render
;;       (fn [{:keys [id ] :as node} b]
;;         ;; (print "edito render  is " c)
;;         [:div.h-100
;;          (if (= -1 (:id node))
;;            [:i.fa.fa-cog.fa-5x.fa-spin]
;;            [:div.h-100 
;;             [title-view node] 
;;             [:div
;;              [:input {:on-change #(reset! t (.-value (.-target %)))
;;                       :value @t}] 
;;              [:b  {:on-click #(dispatch [:file/add-tag id @t])}
;;               "ADD tag"]]
;;             [:button {:on-click #(dispatch [:save-file id @v])} "save file"]
;;             [:div]
;;             [:textarea {:on-change #(do
;;                                       (reset! v (.-value (.-target %)))
;;                                       (reset! edited true))
;;                         :value @v}
;;              ]])])})))
(defn editor
  [node c]
  (let [tag (r/atom "")]
    (fn [{id :id :as node} c]
      [:div.h-100
       (if (= -1 (:id node))
         [:i.fa.fa-cog.fa-5x.fa-spin]
         [:div.h-100 
          [title-view node] 
          [content-view node c]  ])])   ))

(defn tag-view
  [cid tid]
  (let [show (r/atom false)
        tag (subscribe [:tag tid])]
    (fn [cid tid]
      [:span.small-tag.mr-2 {:on-mouse-over #(reset! show true)
                             :on-mouse-out #(reset! show false)}
       [:i.px-1 (:name @tag)]
       [:a.delete-tag {}
        [:i.fa.fa-close {:on-click #(dispatch [:file/remove-tag cid (:id @tag)])
                         :style {:opacity (if @show 1 0)}}]]]     )))

(defn tags-edit
  []
  (let [tags-name (subscribe [:tags-name])
        cid (subscribe [:current-node-id])
        node-tags (subscribe [:current-node-tags ])
        edit (r/atom "" )]
    (fn []
      [:div.tags-edit
       (doall (for [tag @node-tags]
               ^{:key tag}[tag-view @cid tag]))
       [:input {:value @edit
                :on-change #(reset! edit (u/event-value %))
                :on-key-down #(if (= (.-key %) "Enter" )
                                (dispatch [:file/add-tag @cid @edit]))}]])))

(defn compose-panel
  []
  (let [node (subscribe [:current-node])
        b (subscribe [:buffer (:id @node)])]
    (print "current buffer:" @b)
    [scrollbars {:style {:flex 1
                         :height "100%"}}
     [:div {:style {:flex 1 }
            :data-id (:id @node)}
      [tags-edit ]
      [editor @node (:content @b)]]])) 
