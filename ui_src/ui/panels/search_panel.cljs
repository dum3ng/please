(ns ui.panels.search-panel
  (:require [re-frame.core :refer [dispatch subscribe]]
            [reagent.core :as r]
            [ui.components.core :as c]
            [clojure.string :as string]
            [ui.utils :as u]))

(defn search-input
  []
  (let [token (r/atom "")]
    (fn []
      [:div.search-input
       [:input {:on-change #(reset! token (u/event-value %))
                :on-key-down #(if (= "Enter" (.-key %))
                                (dispatch [:search/set-token @token])  )
                :value @token}]])))

(defn result-view
  [item]
  (let [[filename lines] item
        filename (-> filename (string/split #"/") last)
        id (u/get-id filename)
        node (subscribe [:node/by-id id])]
    [:div.list-group-item {:on-click #(dispatch [:node/select id])}
     [:p [:i.fa.fa-file-text][:b (:name @node)]]
     (for [[_ line] lines]
       ;; FIXME the id should be..
       ^{:key (:line-number line)}
       [:p {:style {:color "red"}} (:line-number line)
        (:content line)])]))

(defn search-panel
  []
  (let [result (subscribe [:search-result])
        token (r/atom "")
        searching (subscribe [:searching])]
    (fn []
      [:div.panel.search-panel
       [search-input]
       [:div
        (if @searching 
          [:i.fa.fa-cog.fa-pulse])
        [:span
         [:b (reduce #(+ %1 (count (last %2))) 0 @result)]  " matches, in " [:b (str (count @result))]
         " files"]
        [c/list-group {}
         (for [[filename lines] @result]
           ^{:keys filename}[result-view [filename lines]])]]])))
