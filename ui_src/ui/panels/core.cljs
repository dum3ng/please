(ns ui.panels.core
  (:require [ui.panels.compose-panel :as c  ]
            [ui.panels.files-panel :as f  ]
            [ui.panels.search-panel :as s]))

(def compose-panel c/compose-panel )

(def files-panel f/files-panel)

(def search-panel s/search-panel)
