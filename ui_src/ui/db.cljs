(ns ui.db
  (:require [ui.utils :as u]))


;; (def  node
;;   "a node can be a file or folder"
;;   {:id (str (random-uuid))
;;    :type "folder"
;;    :parent-id "..."
;;    :children [all ids]})
(defn create-sorted-map
  "use the :name in value map as sortor"
  [m]
  (into (sorted-map-by (fn [k1 k2]
                         (compare [(:name (get m k1)) k1]
                                  [(:name (get m k2)) k2])))
        m))

(defonce id (atom 0))

(defn gen-id []
  (swap! id inc))

(defn create-node
  [{:keys [id name tags] :or {id (gen-id) 
                              name "untitled"
                              tags #{}}}]
  {:id id
   :name name
   :tags #{}})

(defn create-tag
  [n]
  {:id (gen-id)
   :name n
   :nodes #{}})
;; (def first-id (str (random-uuid)))
;; (defn fake-tree
;;   []
;;   (let [id0  (gen-id)
;;         id1 (gen-id) 
;;         id2 (gen-id)
;;         id3 (gen-id)
;;         n0 (create-node {:id id0 :name "folder1" :node-type :folder})
;;         n1 (create-node {:id id1 :name "file1" :id-path [id0]})
;;         n2 (create-node {:id id2 :type "folder" :name "folder11" })

;;         n3 (create-node {:id id3 :name "file3"  })
;;         ]
;;     {id0 (-> n0
;;              (assoc-in  [:children id1] n1)
;;              (assoc-in [:children id2] n2))
;;      id3 n3}
;;     ))
(defn fake-tree
  []
  (let [id0 (gen-id)
        id1 (gen-id)
        id2 (gen-id)
        td0 (gen-id)
        td1 (gen-id)
        td2 (gen-id)]
    {:nodes (create-sorted-map  {id0 {:id id0
                                      :name "file0"
                                      :tags #{td0 td1}}
                                 id1 {:id id1
                                      :name "file1"
                                      :tags #{td1}}
                                 id2 {:id id2
                                      :name "file2"
                                      :tags #{td2}}})
     :tags (create-sorted-map {td0 {:id td0
                                    :name "tech"
                                    :nodes #{id0}}
                               td1 {:id td1
                                    :name "life"
                                    :nodes #{id1 id0}}
                               td2 {:id td2
                                    :name "hack"
                                    :nodes #{id2}}})}))

(def default-db
  (merge (fake-tree)
         {:root "/Users/dumeng/temp/please"
          :current-node-id ""
          :buffers {}}))


(defonce db (atom {}))

(defn init-db!
  []
  (let [info (u/get-info)
        _db (:db info)
        _db (-> _db
                (assoc :flash {})
                (assoc :buffers {})
                (assoc :search {
                                :process #js{}
                                :searching false
                                :token ""
                                :output ""
                                :result {}}))
        _id (:id info)]
    
    (reset! id _id )
    (reset! db _db)))
