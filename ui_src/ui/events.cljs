(ns ui.events
  (:require [re-frame.core :refer [dispatch after debug reg-event-db reg-event-fx reg-fx]]
            [ui.db :refer [default-db ]
             :as db]
            [ui.utils :as u]
            [ui.effects]))

(def path (js/require "path"))

(reg-event-db
 :log
 (fn [db _]
   (cljs.pprint/pprint db)
   db))

(reg-event-db
 :db/initialize-prod
 (fn [_ _]
   (deref db/db)))

(reg-event-db
 :db/initialize
 (fn [db [_ ]]
   default-db))


(defn node-by-id
  [db id]
  (-> db :nodes (get id)))



;; file maniputation
(reg-event-fx
 :node/add
 [debug]
 (fn [{db :db} [_  ]]
   (print "add file")
   (let [{:keys [id name ] :as node} (db/create-node {})]
     {:db (-> db
              (assoc-in  [:nodes id] node)
              (assoc-in [:buffers id] "")
              (assoc :current-node-id id))
      :create-file {:filepath (u/file-path
                               (:root db) (u/file-name id [] name ))} })))

(reg-event-fx
 :node/delete
 (fn [{db :db} [_ id]]
   (let [node (get-in db [:nodes id])
         tags-id (:tags node)
         tags (mapv #(get-in db [:tags %]) tags-id)
         tags (mapv #(assoc % :nodes (disj (:nodes %) id))
                    tags)]
     ;; (dleete the file)
     {:db  (reduce #(assoc-in %1 [:tags (:id %2)] %2)
                   (assoc db :nodes (dissoc (:nodes db) id)) tags
                   )
      :delete-file {:filepath (u/file-path (:root db)
                                           (u/file-name id tags (:name node)))}})))

;; MARK read and write
(reg-event-db
 :file/read-success
 (fn [db [_ node c]]
   (print "read success:" c)
   (let [cid (:current-node-id db)] 
     (-> db
         (assoc-in [:buffers (:id node)  ] {:content  c})
         (assoc :flash {:message "read success"
                        :open true})
         (u/assoc-if (= cid -1)
                     :current-node-id (:id node))))))

(reg-event-db
 :file/read-failure
 (fn [db [_ node msg]]
   (print "read failure: " msg)
   (-> db
       (assoc :flash {:message "read file failure"
                      :open true
                      :error msg}))))

(reg-event-fx
 :node/select
 (fn [{db :db} [_ id]]
   (let [node (get-in db [:nodes id])]
     (if (get-in db [:buffers id])
       {:db      (assoc db :current-node-id id)}
       (let  [{:keys [tags name]} node
              tags (mapv #(get-in db [:tags % :name]) tags)]
         {:db (assoc db :current-node-id -1)
          :read-file  {:id id
                       :filename (.join path (:root db)
                                        (u/file-name id tags (:name node)))
                       :on-success [:file/read-success node ]
                       :on-failure [:file/read-failure node]}})))))

(defn log
  [db]
  (print db))
(def log-interceptor (after log))

;;MARK  file edit
(reg-event-db
 :buffer/save
 [log-interceptor]
 (fn [db [_ id c]]
   (print "buffer to save : " c)
   (assoc-in db [:buffers id :content] c)))

(reg-event-fx
 :buffer/save-file
 [log-interceptor]
 (fn [{db :db} [_ id c]]
   (let [node (get-in db [:nodes id])
         tagsname (u/tag-names db node)]
     {:db db
      :dispatch [:buffer/save id c]
      :save-file {:filename (u/file-path (:root db)
                                         (u/file-name id tagsname (:name node))) 
                  :content c
                  :on-success [:file/save-success node]
                  :on-failure [:file/save-failure node]}})))

#_(reg-event-fx
 :save-file
 (fn [{db :db} [_ id ]]
   (let [b (get-in db [:buffers id])
         node (get-in db [:nodes id])
         {:keys [tags name]} node
         tags (mapv #(get-in db [:tags % :name]) tags)]
     (print "tags :" tags)
     {:db db
      :save-file {:filename (.join path (:root db)
                                   (u/file-name id tags (:name node)))
                  :content b
                  :on-success [:file/save-success node]
                  :on-failure [:file/save-failure node]}})))


(reg-event-db
 :file/save-success
 (fn [db [_ node]]
   (assoc db :flash {:open true
                     :message "save file success"})))

(reg-event-db
 :file/save-failure
 (fn [db [_ node msg]]
   (assoc db :flash {:message "save failure"
                     :open true
                     :error msg
                     :node node})))

;; change file title (name)
(reg-event-fx
 :file/set-title
 (fn [{db :db} [_ id title]]
   (let [node (get-in db [:nodes id])
         tagnames (u/tag-names db node)]
     {:db (assoc-in db [:nodes id :name] title)
      :rename-file {:oldname (u/file-path (:root db)
                                          (u/file-name id tagnames (:name node)))
                    :newname (u/file-path (:root db)
                                          (u/file-name id tagnames title))}})))

;;MARK tags


(reg-event-fx
 :file/add-tag
 (fn [{db :db} [_ id tag-name]]
   (let [tag (or (some #(if (= tag-name (:name (last %))) (last %) ) (:tags db))
                 (db/create-tag tag-name))
         node (get-in db [:nodes id])
         tags-id (:tags node)
         tagnames  (mapv #(get-in db [:tags % :name]) tags-id) ]
     (u/assoc-if
      {:db (-> db
               (assoc-in [:tags (:id tag) ] tag)
               (update-in  [:nodes id :tags] conj (:id tag))
               (update-in [:tags (:id tag) :nodes] conj id))}
      (not (contains? tags-id (:id tag)))
      :rename-file 
      {:oldname (u/file-path (:root db) (u/file-name id tagnames (:name node) ))
       :newname (u/file-path (:root db) (u/file-name id (conj tagnames tag-name) (:name node) ))}
      ))
   ))

;;TODO if tags has 0 nodes, should remove this tag.
(reg-event-fx
 :file/remove-tag
 (fn [{db :db} [_ id tag-id]]
   (let [node (get-in db [:nodes id])
         tags-id (:tags node)
         tagnames  (mapv #(get-in db [:tags % :name]) tags-id)
         newtagnames (mapv #(get-in db [:tags % :name]) (disj tags-id tag-id))]
     {:db (-> db
              (update-in  [:nodes id :tags] disj tag-id)
              (update-in [:tags tag-id :nodes ] disj id))
      :rename-file {:oldname (u/file-path (:root db) (u/file-name id tagnames (:name node)))
                    :newname (u/file-path (:root db)(u/file-name id newtagnames (:name node)))}})))



;;MARK search 



;;MARK flash
(reg-event-db 
 :flash/close
 (fn [db _]
   (assoc-in db [:flash :open] false)))

;;search
(reg-event-fx
 :search/start
 (fn [{db :db} [_ token]]
   (let [search (:search db)
         {:keys [searching ]} search
         process (u/spawn-search token (:root db))
         process (doto process
                   (->
                    (aget "stdout")
                    (.on  "data" #(dispatch [:search/receive-output %])))
                   (.on "close" #(if (not %2)
                                   (dispatch [:search/stop]))))]
     {:db (-> db
              (assoc-in [:search :searching ] true))})))

;; receive search output
(reg-event-fx
 :search/receive-output
 (fn [{db :db} [_ data]]
   ;; FIXME the parse output may not be in the expected sequence? 
   {:db (update-in db [:search :output] str data)
    :parse-output {:data data }}))


;; receive search result
;; search result is
;; {"filename0" {1 {:line-number 1
;;              :start-col 2
;;              :end-col 10
;;            :content}}}

;; receive the parsed result
(reg-event-db
 :search/receive-result
 (fn [db [_ res]]
   (update-in db [:search :result]
              merge res)))

;; search end
(reg-event-db
 :search/stop
 (fn [db _]
   (assoc-in db [:search :searching ] false)))
;; (def )
;;set search token
(reg-event-fx
 :search/set-token
 (fn [{db :db}[_ token]]
   (let [old-token (get-in db [:search :token])
         {:keys [searching process] :as search} (:search db)]
     (if (= old-token token)
       {:db db}
       (u/assoc-if 
        {:db (-> db
                 (assoc :search {:process #js{}
                                 :token token
                                 :searching true
                                 :output ""
                                 :result {}}))
         :dispatch [:search/start token]}
        searching :kill-process process
         )))))


;; MARK app
;; save config

;; save db
(reg-event-fx
 :db/save
 (fn [{db :db} [_ ]]
   {:db (assoc db :db-saving true)
    :save {:on-success [:db/save-success]
              :on-failure [:db/save-failure]
              :db db
              :id (deref db/id)
              :filepath (u/file-path (:root db) "__please")}}))

(reg-event-db
 :db/save-success
 (fn [db [_]]
   (-> db
       (assoc :db-saving false)
       (assoc :flash {:message "db saved success"}))))

(reg-event-db
 :db/save-failure
 (fn [db _]
   (-> db
       (assoc :db-saving false)
       (assoc :flash {:message "db saved FAILURE"}))))
