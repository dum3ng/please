(ns ui.components.core
  (:require [reagent.core :as r]
            [cljsjs.react.custom.scrollbars]
            [cljsjs.react-bootstrap]))

(defn divider
  [& {:as style} ]
  [:div {:style (merge  {:width "100%"
                         :height 1
                         :background "gray"
                         :margin 0
                         :padding 0}
                        style)}])

(def scrollbars
  (-> js/ReactCustomScrollbars
      (aget "Scrollbars")
      r/adapt-react-class))

(defn getrb 
  [s]
  (-> js/ReactBootstrap
      (aget s)
      r/adapt-react-class))

(def list-group (getrb "ListGroup") )
(def list-group-item (getrb "ListGroupItem") )
