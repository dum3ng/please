(ns ui.components.core
  (:require [clojure.string :as string]))


(defn dash->camel [dashed]
  (let [[start & parts](-> dashed (string/split #"-"))]
    (apply str start (map string/capitalize parts))))

#_(defmacro defrb
  ([n]
   (let [s (dash->camel n)]
     (defrb n s)))
  ([n s]
   `(defn )))
