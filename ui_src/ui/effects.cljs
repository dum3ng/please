(ns ui.effects
  (:require [re-frame.core :refer [reg-fx dispatch]]
            [re-frame.router :as router]
            [ui.utils :as u]
            [clojure.string :as string]))


(def fs (js/require "fs"))

(reg-fx
 :save-file
 (fn [value]
   (let [{:keys [filename content on-success on-failure]} value]
     (.writeFile fs filename content
                 #(if % (dispatch (conj on-failure (aget % "message")))
                      (dispatch on-success ))) )   ))


(reg-fx
 :read-file
 (fn [value]
   (let [{:keys [filename on-success on-failure]} value]
     (.readFile fs filename "utf-8" (fn [err data]
                     (if err
                       (dispatch (conj on-failure (aget err "message")))
                       (dispatch (conj on-success data))))))))


(reg-fx
 :rename-file
 (fn [value]
   (let [{:keys [oldname newname ]} value]
     (.rename fs oldname newname #()))))


(reg-fx
 :create-file
 (fn [value]
   (.writeFileSync fs (:filepath value) "")))

(reg-fx
 :delete-file
 (fn [v]
   (.unlinkSync fs (:filepath v))))


(reg-fx
 :save
 (fn [v]
   (let [{:keys [on-success on-failure id db filepath]} v
         db (-> db
                (dissoc :flash)
                (dissoc :buffers)
                (dissoc :search))
         config {:root (:root db) :id id}]
     (.writeFile fs filepath (str db) #(if %
                                (dispatch on-failure)
                                (dispatch on-success)))
     (.writeFile fs (u/config-path) (str config)
                 #()))))


(reg-fx
 :kill-process
 (fn [p]
   (.kill p)))


;; output is ackmate format
;;  :filename
;;  linenum;startcol endcol:linecontent
(reg-fx
 :parse-output
 (fn [v]
   (let [data (:data v)
         lines (string/split-lines data)
         res (reduce #(let [res (u/parse %2)
                        {:keys [type parsed]} res]
                    (case type
                      :filename (-> %1
                                    (assoc  ::current-file parsed)
                                    (assoc parsed {}))
                      :detail (update %1 (::current-file %1)
                                      assoc (:line-number parsed) parsed)
                      %1))
                     {::current-file nil} lines)]
     (dispatch [:search/receive-result (dissoc res ::current-file)]))))
