(ns ui.subs
  (:require [re-frame.core :refer [reg-sub reg-event-db reg-event-fx]]
            [ui.utils :as u]))

(reg-sub
 :db
 (fn [db _]
   db))
 
(reg-sub
 :current-node-id
 (fn [db _]
   (:current-node-id db)))

(reg-sub
 :node/name-by-id
 (fn [db [_ id]]
   (-> db
       :nodes
       (get id)
       :name)))

(reg-sub
 :node/by-id
 (fn [db [_ id]]
   (-> db
       :nodes
       (get id))))

(reg-sub
 :current-node
 (fn [db _]
   (-> db :nodes (get (:current-node-id db) {:id -1})
       )))
;; (reg-sub
;;  :current-node
;;  :<- [:tree]
;;  :<- [:current-node-id]
;;  (fn [[tree id] _]
;;    (get tree id)))



(reg-sub
 :nodes
 (fn [db _]
   (:nodes db)))

(reg-sub
 :nodes-with-tags
 (fn [db [_ tags-id]]
   (let [tags (mapv #(get-in db [:tags %] ) tags-id)
         nodes-id (reduce #(clojure.set/union %1 (set (:nodes %2)))  #{} tags)
         ]
     (select-keys (:nodes db) nodes-id))))

(reg-sub
 :node
 (fn [db [_ id]] 
   (-> db :nodes (get id))))

;; MARK tag

(reg-sub
 :tag
 (fn [db [_ id]]
   (get-in db [:tags id])))

(reg-sub
 :tags-name-of-node
 (fn [db [_ id]]
   (let [node (get-in db [:nodes id])]
     (u/tag-names db node))))

(reg-sub
 :current-node-tags-name
 (fn [db _]
   (let [cid (:current-node-id db)]
     (if-let [node (get-in db [:nodes cid])]
       (u/tag-names db node) 
       []))))

(reg-sub
 :current-node-tags
 (fn [db _]
   (let [cid (:current-node-id db)]
     (if-let [node (get-in db [:nodes cid])]
       (:tags node) 
       []))))
;; tags
(reg-sub
 :tags-name
 (fn [db _]
   (mapv #(:name (last %))
    (:tags db))))


(reg-sub
 :buffer
 (fn [db [_ id]]
   (-> db :buffers (get id))))

(reg-sub
 :flash
 (fn [db _]
   (get  db :flash {})))


(reg-sub
 :search-result
 (fn [db _]
   (get-in db [:search :result])))

(reg-sub
 :searching
 (fn [db _]
   (get-in db [:search :searching])))
