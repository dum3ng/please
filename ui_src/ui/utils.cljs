(ns ui.utils
  (:require [cljs.reader :as reader]
            [clojure.string :as s]
            [clojure.string :as string]))

(def child-process (js/require "child_process"))

(def spawn (aget child-process "spawn"))

(def os (js/require "os"))

(def path (js/require "path"))

(def fs (js/require "fs"))

(declare default-config home-dir)

(defn config-path
  []
  (.join path (home-dir) ".please"))


;; TODO write default content when the config file does not exist
(defn read-config
  []
  (let [file  (config-path)]
    (try
      (let [config (.readFileSync fs file "utf-8")]
        (reader/read-string config))
      (catch js/Object err
        (let [d (default-config)]
         (.writeFileSync fs (str d) ) 
          d)))))

(defn read-root
  [root]
  (let [root-file (.join path root "__please")
        db (.readFileSync fs root-file "utf-8")
        db (reader/read-string db)]
    db))

(defn get-info
  []
  (let [config (read-config)
        root (:root config)
        id (:id config)
        db (read-root root)]
    {:db db
     :id id}))


(defn file-name
  [id tags name]
  (str id "_" (s/join "." tags) "_" name ".md")  )

(defn file-path
  [root name]
  (.join path root name))

;; FIXME this will not work since the tag and title may contain
;; the `_` character
(defn parse-file-name
  [f] 
  (let [[_ id tags name] (re-matches #"([0-9]+)_(.*)_(.+)" f)
        tags (if (s/blank? tags)
               []
               (s/split tags "."))]
    {:id id
     :tags tags
     :name name}))

(defn assoc-if
  [ m pred k v]
  (if pred
    (assoc m k v)
    m))

(defn assoc-in-if
  [m pred  ks v]
  (if pred
    (assoc-in m ks v)
    m))

;; TODO the ag executable should be the embed in path
(defn spawn-search
  [token path]
  (spawn "ag" #js["--markdown" "--ackmate" token path]))


(defn event-value
  [e]
  (.-value (.-target e)))

(defn parse
  [line]
  (cond
    (s/starts-with? line ":") {:type :filename
                               :parsed (subs line 1)}
    (s/blank? line) {}
    :else (let [[_ line-number start-col end-col content] (re-matches #"([0-9]+);([0-9]+)\s([0-9]+):(.+)" line)]
            {:type :detail
             :parsed {:line-number line-number
                      :start-col start-col
                      :end-col end-col
                      :content content}}) )
  )


#_(defn daunt
  [delay handler can-do]
  (let [t (js/setTimeout
           #(if can-do
              (handler)
              (do (js/clearTimeout t)))
           delay)]))

(defn tag-names
  [db node]
  (let [ tags-id (:tags node)
        tagnames  (mapv #(get-in db [:tags % :name]) tags-id)]
    tagnames))

(defn which-system
  []
  (case js/process.platform
    "darwin" :osx
    "win32" :windows
    :linux) )   

(defn home-dir
  []
  (.homedir os))




(defn default-config
  []
  {:id 0
   ;; FIXME should be empty. ask user to config when first use
   :root (.join path (home-dir) "temp/please")})

(defn trim-ext
  [s]
  (if (string/ends-with? ".md")
    (subs s 0 (- (count s) 3))
    s))

(defn get-id
  [s]
  (js/parseInt (re-find #"[0-9]+" s) 10))
