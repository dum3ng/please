(ns ui.fake-content)

(defn content
  [node]
  (str "my id is " (:id node)
       "and I have tags  " (:tags node)))
