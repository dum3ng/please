(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["react"], factory);
	else if(typeof exports === 'object')
		exports["ReactCustomScrollbars"] = factory(require("react"));
	else
		root["ReactCustomScrollbars"] = factory(root["React"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.Scrollbars = undefined;

	var _Scrollbars = __webpack_require__(4);

	var _Scrollbars2 = _interopRequireDefault(_Scrollbars);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

	exports["default"] = _Scrollbars2["default"];
	exports.Scrollbars = _Scrollbars2["default"];

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var prefix = __webpack_require__(11)
	var toCamelCase = __webpack_require__(12)
	var cache = { 'float': 'cssFloat' }
	var addPxToStyle = __webpack_require__(10)

	function style (element, property, value) {
	  var camel = cache[property]
	  if (typeof camel === 'undefined') {
	    camel = detect(property)
	  }

	  // may be false if CSS prop is unsupported
	  if (camel) {
	    if (value === undefined) {
	      return element.style[camel]
	    }

	    element.style[camel] = addPxToStyle(camel, value)
	  }
	}

	function each (element, properties) {
	  for (var k in properties) {
	    if (properties.hasOwnProperty(k)) {
	      style(element, k, properties[k])
	    }
	  }
	}

	function detect (cssProp) {
	  var camel = toCamelCase(cssProp)
	  var result = prefix(camel)
	  cache[camel] = cache[cssProp] = cache[result] = result
	  return result
	}

	function set () {
	  if (arguments.length === 2) {
	    each(arguments[0], arguments[1])
	  } else {
	    style(arguments[0], arguments[1], arguments[2])
	  }
	}

	module.exports = set
	module.exports.set = set

	module.exports.get = function (element, properties) {
	  if (Array.isArray(properties)) {
	    return properties.reduce(function (obj, prop) {
	      obj[prop] = style(element, prop || '')
	      return obj
	    }, {})
	  } else {
	    return style(element, properties || '')
	  }
	}


/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	exports.renderViewDefault = renderViewDefault;
	exports.renderTrackHorizontalDefault = renderTrackHorizontalDefault;
	exports.renderTrackVerticalDefault = renderTrackVerticalDefault;
	exports.renderThumbHorizontalDefault = renderThumbHorizontalDefault;
	exports.renderThumbVerticalDefault = renderThumbVerticalDefault;

	var _react = __webpack_require__(2);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

	function renderViewDefault(props) {
	    return _react2["default"].createElement('div', props);
	}

	function renderTrackHorizontalDefault(_ref) {
	    var style = _ref.style;

	    var props = _objectWithoutProperties(_ref, ['style']);

	    var finalStyle = _extends({}, style, {
	        right: 2,
	        bottom: 2,
	        left: 2,
	        borderRadius: 3,
	        cursor: 'pointer'
	    });
	    return _react2["default"].createElement('div', _extends({ style: finalStyle }, props));
	}

	function renderTrackVerticalDefault(_ref2) {
	    var style = _ref2.style;

	    var props = _objectWithoutProperties(_ref2, ['style']);

	    var finalStyle = _extends({}, style, {
	        right: 2,
	        bottom: 2,
	        top: 2,
	        borderRadius: 3,
	        cursor: 'pointer'
	    });
	    return _react2["default"].createElement('div', _extends({ style: finalStyle }, props));
	}

	function renderThumbHorizontalDefault(_ref3) {
	    var style = _ref3.style;

	    var props = _objectWithoutProperties(_ref3, ['style']);

	    var finalStyle = _extends({}, style, {
	        cursor: 'pointer',
	        borderRadius: 'inherit',
	        backgroundColor: 'rgba(0,0,0,.2)'
	    });
	    return _react2["default"].createElement('div', _extends({ style: finalStyle }, props));
	}

	function renderThumbVerticalDefault(_ref4) {
	    var style = _ref4.style;

	    var props = _objectWithoutProperties(_ref4, ['style']);

	    var finalStyle = _extends({}, style, {
	        cursor: 'pointer',
	        borderRadius: 'inherit',
	        backgroundColor: 'rgba(0,0,0,.2)'
	    });
	    return _react2["default"].createElement('div', _extends({ style: finalStyle }, props));
	}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _raf2 = __webpack_require__(15);

	var _raf3 = _interopRequireDefault(_raf2);

	var _domCss = __webpack_require__(1);

	var _domCss2 = _interopRequireDefault(_domCss);

	var _react = __webpack_require__(2);

	var _react2 = _interopRequireDefault(_react);

	var _getScrollbarWidth = __webpack_require__(8);

	var _getScrollbarWidth2 = _interopRequireDefault(_getScrollbarWidth);

	var _returnFalse = __webpack_require__(9);

	var _returnFalse2 = _interopRequireDefault(_returnFalse);

	var _getInnerWidth = __webpack_require__(7);

	var _getInnerWidth2 = _interopRequireDefault(_getInnerWidth);

	var _getInnerHeight = __webpack_require__(6);

	var _getInnerHeight2 = _interopRequireDefault(_getInnerHeight);

	var _styles = __webpack_require__(5);

	var _defaultRenderElements = __webpack_require__(3);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

	exports["default"] = (0, _react.createClass)({

	    displayName: 'Scrollbars',

	    propTypes: {
	        onScroll: _react.PropTypes.func,
	        onScrollFrame: _react.PropTypes.func,
	        onScrollStart: _react.PropTypes.func,
	        onScrollStop: _react.PropTypes.func,
	        renderView: _react.PropTypes.func,
	        renderTrackHorizontal: _react.PropTypes.func,
	        renderTrackVertical: _react.PropTypes.func,
	        renderThumbHorizontal: _react.PropTypes.func,
	        renderThumbVertical: _react.PropTypes.func,
	        autoHide: _react.PropTypes.bool,
	        autoHideTimeout: _react.PropTypes.number,
	        autoHideDuration: _react.PropTypes.number,
	        thumbSize: _react.PropTypes.number,
	        thumbMinSize: _react.PropTypes.number,
	        universal: _react.PropTypes.bool,
	        style: _react.PropTypes.object,
	        children: _react.PropTypes.node
	    },

	    getDefaultProps: function getDefaultProps() {
	        return {
	            renderView: _defaultRenderElements.renderViewDefault,
	            renderTrackHorizontal: _defaultRenderElements.renderTrackHorizontalDefault,
	            renderTrackVertical: _defaultRenderElements.renderTrackVerticalDefault,
	            renderThumbHorizontal: _defaultRenderElements.renderThumbHorizontalDefault,
	            renderThumbVertical: _defaultRenderElements.renderThumbVerticalDefault,
	            autoHide: false,
	            autoHideTimeout: 1000,
	            autoHideDuration: 200,
	            thumbMinSize: 30,
	            universal: false
	        };
	    },
	    getInitialState: function getInitialState() {
	        return {
	            didMountUniversal: false
	        };
	    },
	    componentDidMount: function componentDidMount() {
	        this.addListeners();
	        this.update();
	        this.componentDidMountUniversal();
	    },
	    componentDidMountUniversal: function componentDidMountUniversal() {
	        // eslint-disable-line react/sort-comp
	        var universal = this.props.universal;

	        if (!universal) return;
	        this.setState({ didMountUniversal: true });
	    },
	    componentDidUpdate: function componentDidUpdate() {
	        this.update();
	    },
	    componentWillUnmount: function componentWillUnmount() {
	        this.removeListeners();
	        (0, _raf2.cancel)(this.requestFrame);
	        clearTimeout(this.hideTracksTimeout);
	        clearInterval(this.detectScrollingInterval);
	    },
	    getScrollLeft: function getScrollLeft() {
	        var view = this.refs.view;

	        return view.scrollLeft;
	    },
	    getScrollTop: function getScrollTop() {
	        var view = this.refs.view;

	        return view.scrollTop;
	    },
	    getScrollWidth: function getScrollWidth() {
	        var view = this.refs.view;

	        return view.scrollWidth;
	    },
	    getScrollHeight: function getScrollHeight() {
	        var view = this.refs.view;

	        return view.scrollHeight;
	    },
	    getClientWidth: function getClientWidth() {
	        var view = this.refs.view;

	        return view.clientWidth;
	    },
	    getClientHeight: function getClientHeight() {
	        var view = this.refs.view;

	        return view.clientHeight;
	    },
	    getValues: function getValues() {
	        var view = this.refs.view;
	        var scrollLeft = view.scrollLeft;
	        var scrollTop = view.scrollTop;
	        var scrollWidth = view.scrollWidth;
	        var scrollHeight = view.scrollHeight;
	        var clientWidth = view.clientWidth;
	        var clientHeight = view.clientHeight;


	        return {
	            left: scrollLeft / (scrollWidth - clientWidth) || 0,
	            top: scrollTop / (scrollHeight - clientHeight) || 0,
	            scrollLeft: scrollLeft,
	            scrollTop: scrollTop,
	            scrollWidth: scrollWidth,
	            scrollHeight: scrollHeight,
	            clientWidth: clientWidth,
	            clientHeight: clientHeight
	        };
	    },
	    getThumbHorizontalWidth: function getThumbHorizontalWidth() {
	        var _props = this.props;
	        var thumbSize = _props.thumbSize;
	        var thumbMinSize = _props.thumbMinSize;
	        var _refs = this.refs;
	        var view = _refs.view;
	        var trackHorizontal = _refs.trackHorizontal;
	        var scrollWidth = view.scrollWidth;
	        var clientWidth = view.clientWidth;

	        var trackWidth = (0, _getInnerWidth2["default"])(trackHorizontal);
	        var width = clientWidth / scrollWidth * trackWidth;
	        if (trackWidth === width) return 0;
	        if (thumbSize) return thumbSize;
	        return Math.max(width, thumbMinSize);
	    },
	    getThumbVerticalHeight: function getThumbVerticalHeight() {
	        var _props2 = this.props;
	        var thumbSize = _props2.thumbSize;
	        var thumbMinSize = _props2.thumbMinSize;
	        var _refs2 = this.refs;
	        var view = _refs2.view;
	        var trackVertical = _refs2.trackVertical;
	        var scrollHeight = view.scrollHeight;
	        var clientHeight = view.clientHeight;

	        var trackHeight = (0, _getInnerHeight2["default"])(trackVertical);
	        var height = clientHeight / scrollHeight * trackHeight;
	        if (trackHeight === height) return 0;
	        if (thumbSize) return thumbSize;
	        return Math.max(height, thumbMinSize);
	    },
	    getScrollLeftForOffset: function getScrollLeftForOffset(offset) {
	        var _refs3 = this.refs;
	        var view = _refs3.view;
	        var trackHorizontal = _refs3.trackHorizontal;
	        var scrollWidth = view.scrollWidth;
	        var clientWidth = view.clientWidth;

	        var trackWidth = (0, _getInnerWidth2["default"])(trackHorizontal);
	        var thumbWidth = this.getThumbHorizontalWidth();
	        return offset / (trackWidth - thumbWidth) * (scrollWidth - clientWidth);
	    },
	    getScrollTopForOffset: function getScrollTopForOffset(offset) {
	        var _refs4 = this.refs;
	        var view = _refs4.view;
	        var trackVertical = _refs4.trackVertical;
	        var scrollHeight = view.scrollHeight;
	        var clientHeight = view.clientHeight;

	        var trackHeight = (0, _getInnerHeight2["default"])(trackVertical);
	        var thumbHeight = this.getThumbVerticalHeight();
	        return offset / (trackHeight - thumbHeight) * (scrollHeight - clientHeight);
	    },
	    scrollLeft: function scrollLeft() {
	        var left = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
	        var view = this.refs.view;

	        view.scrollLeft = left;
	    },
	    scrollTop: function scrollTop() {
	        var top = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
	        var view = this.refs.view;

	        view.scrollTop = top;
	    },
	    scrollToLeft: function scrollToLeft() {
	        var view = this.refs.view;

	        view.scrollLeft = 0;
	    },
	    scrollToTop: function scrollToTop() {
	        var view = this.refs.view;

	        view.scrollTop = 0;
	    },
	    scrollToRight: function scrollToRight() {
	        var view = this.refs.view;

	        view.scrollLeft = view.scrollWidth;
	    },
	    scrollToBottom: function scrollToBottom() {
	        var view = this.refs.view;

	        view.scrollTop = view.scrollHeight;
	    },
	    addListeners: function addListeners() {
	        /* istanbul ignore if */
	        if (typeof document === 'undefined') return;
	        var _refs5 = this.refs;
	        var view = _refs5.view;
	        var trackHorizontal = _refs5.trackHorizontal;
	        var trackVertical = _refs5.trackVertical;
	        var thumbHorizontal = _refs5.thumbHorizontal;
	        var thumbVertical = _refs5.thumbVertical;

	        view.addEventListener('scroll', this.handleScroll);
	        if (!(0, _getScrollbarWidth2["default"])()) return;
	        trackHorizontal.addEventListener('mouseenter', this.handleTrackMouseEnter);
	        trackHorizontal.addEventListener('mouseleave', this.handleTrackMouseLeave);
	        trackHorizontal.addEventListener('mousedown', this.handleHorizontalTrackMouseDown);
	        trackVertical.addEventListener('mouseenter', this.handleTrackMouseEnter);
	        trackVertical.addEventListener('mouseleave', this.handleTrackMouseLeave);
	        trackVertical.addEventListener('mousedown', this.handleVerticalTrackMouseDown);
	        thumbHorizontal.addEventListener('mousedown', this.handleHorizontalThumbMouseDown);
	        thumbVertical.addEventListener('mousedown', this.handleVerticalThumbMouseDown);
	        window.addEventListener('resize', this.handleWindowResize);
	    },
	    removeListeners: function removeListeners() {
	        /* istanbul ignore if */
	        if (typeof document === 'undefined') return;
	        var _refs6 = this.refs;
	        var view = _refs6.view;
	        var trackHorizontal = _refs6.trackHorizontal;
	        var trackVertical = _refs6.trackVertical;
	        var thumbHorizontal = _refs6.thumbHorizontal;
	        var thumbVertical = _refs6.thumbVertical;

	        view.removeEventListener('scroll', this.handleScroll);
	        if (!(0, _getScrollbarWidth2["default"])()) return;
	        trackHorizontal.removeEventListener('mouseenter', this.handleTrackMouseEnter);
	        trackHorizontal.removeEventListener('mouseleave', this.handleTrackMouseLeave);
	        trackHorizontal.removeEventListener('mousedown', this.handleHorizontalTrackMouseDown);
	        trackVertical.removeEventListener('mouseenter', this.handleTrackMouseEnter);
	        trackVertical.removeEventListener('mouseleave', this.handleTrackMouseLeave);
	        trackVertical.removeEventListener('mousedown', this.handleVerticalTrackMouseDown);
	        thumbHorizontal.removeEventListener('mousedown', this.handleHorizontalThumbMouseDown);
	        thumbVertical.removeEventListener('mousedown', this.handleVerticalThumbMouseDown);
	        window.removeEventListener('resize', this.handleWindowResize);
	        // Possibly setup by `handleDragStart`
	        this.teardownDragging();
	    },
	    handleScroll: function handleScroll(event) {
	        var _this = this;

	        var _props3 = this.props;
	        var onScroll = _props3.onScroll;
	        var onScrollFrame = _props3.onScrollFrame;

	        if (onScroll) onScroll(event);
	        this.update(function (values) {
	            var scrollLeft = values.scrollLeft;
	            var scrollTop = values.scrollTop;

	            _this.viewScrollLeft = scrollLeft;
	            _this.viewScrollTop = scrollTop;
	            if (onScrollFrame) onScrollFrame(values);
	        });
	        this.detectScrolling();
	    },
	    handleScrollStart: function handleScrollStart() {
	        var onScrollStart = this.props.onScrollStart;

	        if (onScrollStart) onScrollStart();
	        this.handleScrollStartAutoHide();
	    },
	    handleScrollStartAutoHide: function handleScrollStartAutoHide() {
	        var autoHide = this.props.autoHide;

	        if (!autoHide) return;
	        this.showTracks();
	    },
	    handleScrollStop: function handleScrollStop() {
	        var onScrollStop = this.props.onScrollStop;

	        if (onScrollStop) onScrollStop();
	        this.handleScrollStopAutoHide();
	    },
	    handleScrollStopAutoHide: function handleScrollStopAutoHide() {
	        var autoHide = this.props.autoHide;

	        if (!autoHide) return;
	        this.hideTracks();
	    },
	    handleWindowResize: function handleWindowResize() {
	        this.update();
	    },
	    handleHorizontalTrackMouseDown: function handleHorizontalTrackMouseDown() {
	        var view = this.refs.view;
	        var _event = event;
	        var target = _event.target;
	        var clientX = _event.clientX;

	        var _target$getBoundingCl = target.getBoundingClientRect();

	        var targetLeft = _target$getBoundingCl.left;

	        var thumbWidth = this.getThumbHorizontalWidth();
	        var offset = Math.abs(targetLeft - clientX) - thumbWidth / 2;
	        view.scrollLeft = this.getScrollLeftForOffset(offset);
	    },
	    handleVerticalTrackMouseDown: function handleVerticalTrackMouseDown(event) {
	        var view = this.refs.view;
	        var target = event.target;
	        var clientY = event.clientY;

	        var _target$getBoundingCl2 = target.getBoundingClientRect();

	        var targetTop = _target$getBoundingCl2.top;

	        var thumbHeight = this.getThumbVerticalHeight();
	        var offset = Math.abs(targetTop - clientY) - thumbHeight / 2;
	        view.scrollTop = this.getScrollTopForOffset(offset);
	    },
	    handleHorizontalThumbMouseDown: function handleHorizontalThumbMouseDown(event) {
	        this.handleDragStart(event);
	        var target = event.target;
	        var clientX = event.clientX;
	        var offsetWidth = target.offsetWidth;

	        var _target$getBoundingCl3 = target.getBoundingClientRect();

	        var left = _target$getBoundingCl3.left;

	        this.prevPageX = offsetWidth - (clientX - left);
	    },
	    handleVerticalThumbMouseDown: function handleVerticalThumbMouseDown(event) {
	        this.handleDragStart(event);
	        var target = event.target;
	        var clientY = event.clientY;
	        var offsetHeight = target.offsetHeight;

	        var _target$getBoundingCl4 = target.getBoundingClientRect();

	        var top = _target$getBoundingCl4.top;

	        this.prevPageY = offsetHeight - (clientY - top);
	    },
	    setupDragging: function setupDragging() {
	        (0, _domCss2["default"])(document.body, _styles.disableSelectStyle);
	        document.addEventListener('mousemove', this.handleDrag);
	        document.addEventListener('mouseup', this.handleDragEnd);
	        document.onselectstart = _returnFalse2["default"];
	    },
	    teardownDragging: function teardownDragging() {
	        (0, _domCss2["default"])(document.body, _styles.disableSelectStyleReset);
	        document.removeEventListener('mousemove', this.handleDrag);
	        document.removeEventListener('mouseup', this.handleDragEnd);
	        document.onselectstart = undefined;
	    },
	    handleDragStart: function handleDragStart(event) {
	        this.dragging = true;
	        event.stopImmediatePropagation();
	        this.setupDragging();
	    },
	    handleDrag: function handleDrag(event) {
	        if (this.prevPageX) {
	            var clientX = event.clientX;
	            var _refs7 = this.refs;
	            var view = _refs7.view;
	            var trackHorizontal = _refs7.trackHorizontal;

	            var _trackHorizontal$getB = trackHorizontal.getBoundingClientRect();

	            var trackLeft = _trackHorizontal$getB.left;

	            var thumbWidth = this.getThumbHorizontalWidth();
	            var clickPosition = thumbWidth - this.prevPageX;
	            var offset = -trackLeft + clientX - clickPosition;
	            view.scrollLeft = this.getScrollLeftForOffset(offset);
	        }
	        if (this.prevPageY) {
	            var clientY = event.clientY;
	            var _refs8 = this.refs;
	            var view = _refs8.view;
	            var trackVertical = _refs8.trackVertical;

	            var _trackVertical$getBou = trackVertical.getBoundingClientRect();

	            var trackTop = _trackVertical$getBou.top;

	            var thumbHeight = this.getThumbVerticalHeight();
	            var clickPosition = thumbHeight - this.prevPageY;
	            var offset = -trackTop + clientY - clickPosition;
	            view.scrollTop = this.getScrollTopForOffset(offset);
	        }
	        return false;
	    },
	    handleDragEnd: function handleDragEnd() {
	        this.dragging = false;
	        this.prevPageX = this.prevPageY = 0;
	        this.teardownDragging();
	        this.handleDragEndAutoHide();
	    },
	    handleDragEndAutoHide: function handleDragEndAutoHide() {
	        var autoHide = this.props.autoHide;

	        if (!autoHide) return;
	        this.hideTracks();
	    },
	    handleTrackMouseEnter: function handleTrackMouseEnter() {
	        this.trackMouseOver = true;
	        this.handleTrackMouseEnterAutoHide();
	    },
	    handleTrackMouseEnterAutoHide: function handleTrackMouseEnterAutoHide() {
	        var autoHide = this.props.autoHide;

	        if (!autoHide) return;
	        this.showTracks();
	    },
	    handleTrackMouseLeave: function handleTrackMouseLeave() {
	        this.trackMouseOver = false;
	        this.handleTrackMouseLeaveAutoHide();
	    },
	    handleTrackMouseLeaveAutoHide: function handleTrackMouseLeaveAutoHide() {
	        var autoHide = this.props.autoHide;

	        if (!autoHide) return;
	        this.hideTracks();
	    },
	    showTracks: function showTracks() {
	        var _refs9 = this.refs;
	        var trackHorizontal = _refs9.trackHorizontal;
	        var trackVertical = _refs9.trackVertical;

	        clearTimeout(this.hideTracksTimeout);
	        (0, _domCss2["default"])(trackHorizontal, { opacity: 1 });
	        (0, _domCss2["default"])(trackVertical, { opacity: 1 });
	    },
	    hideTracks: function hideTracks() {
	        if (this.dragging) return;
	        if (this.scrolling) return;
	        if (this.trackMouseOver) return;
	        var autoHideTimeout = this.props.autoHideTimeout;
	        var _refs10 = this.refs;
	        var trackHorizontal = _refs10.trackHorizontal;
	        var trackVertical = _refs10.trackVertical;

	        clearTimeout(this.hideTracksTimeout);
	        this.hideTracksTimeout = setTimeout(function () {
	            (0, _domCss2["default"])(trackHorizontal, { opacity: 0 });
	            (0, _domCss2["default"])(trackVertical, { opacity: 0 });
	        }, autoHideTimeout);
	    },
	    detectScrolling: function detectScrolling() {
	        var _this2 = this;

	        if (this.scrolling) return;
	        this.scrolling = true;
	        this.handleScrollStart();
	        this.detectScrollingInterval = setInterval(function () {
	            if (_this2.lastViewScrollLeft === _this2.viewScrollLeft && _this2.lastViewScrollTop === _this2.viewScrollTop) {
	                clearInterval(_this2.detectScrollingInterval);
	                _this2.scrolling = false;
	                _this2.handleScrollStop();
	            }
	            _this2.lastViewScrollLeft = _this2.viewScrollLeft;
	            _this2.lastViewScrollTop = _this2.viewScrollTop;
	        }, 100);
	    },
	    raf: function raf(callback) {
	        var _this3 = this;

	        if (this.requestFrame) _raf3["default"].cancel(this.requestFrame);
	        this.requestFrame = (0, _raf3["default"])(function () {
	            _this3.requestFrame = undefined;
	            callback();
	        });
	    },
	    update: function update(callback) {
	        var _this4 = this;

	        this.raf(function () {
	            var values = _this4.getValues();
	            if ((0, _getScrollbarWidth2["default"])()) {
	                var _refs11 = _this4.refs;
	                var thumbHorizontal = _refs11.thumbHorizontal;
	                var thumbVertical = _refs11.thumbVertical;
	                var trackHorizontal = _refs11.trackHorizontal;
	                var trackVertical = _refs11.trackVertical;
	                var scrollLeft = values.scrollLeft;
	                var clientWidth = values.clientWidth;
	                var scrollWidth = values.scrollWidth;

	                var trackHorizontalWidth = (0, _getInnerWidth2["default"])(trackHorizontal);
	                var thumbHorizontalWidth = _this4.getThumbHorizontalWidth();
	                var thumbHorizontalX = scrollLeft / (scrollWidth - clientWidth) * (trackHorizontalWidth - thumbHorizontalWidth);
	                var thumbHorizontalStyle = {
	                    width: thumbHorizontalWidth,
	                    transform: 'translateX(' + thumbHorizontalX + 'px)'
	                };
	                var scrollTop = values.scrollTop;
	                var clientHeight = values.clientHeight;
	                var scrollHeight = values.scrollHeight;

	                var trackVerticalHeight = (0, _getInnerHeight2["default"])(trackVertical);
	                var thumbVerticalHeight = _this4.getThumbVerticalHeight();
	                var thumbVerticalY = scrollTop / (scrollHeight - clientHeight) * (trackVerticalHeight - thumbVerticalHeight);
	                var thumbVerticalStyle = {
	                    height: thumbVerticalHeight,
	                    transform: 'translateY(' + thumbVerticalY + 'px)'
	                };
	                (0, _domCss2["default"])(thumbHorizontal, thumbHorizontalStyle);
	                (0, _domCss2["default"])(thumbVertical, thumbVerticalStyle);
	            }
	            if (typeof callback !== 'function') return;
	            callback(values);
	        });
	    },
	    render: function render() {
	        var scrollbarWidth = (0, _getScrollbarWidth2["default"])();
	        var _props4 = this.props;
	        var onScroll = _props4.onScroll;
	        var onScrollFrame = _props4.onScrollFrame;
	        var onScrollStart = _props4.onScrollStart;
	        var onScrollStop = _props4.onScrollStop;
	        var renderView = _props4.renderView;
	        var renderTrackHorizontal = _props4.renderTrackHorizontal;
	        var renderTrackVertical = _props4.renderTrackVertical;
	        var renderThumbHorizontal = _props4.renderThumbHorizontal;
	        var renderThumbVertical = _props4.renderThumbVertical;
	        var autoHide = _props4.autoHide;
	        var autoHideTimeout = _props4.autoHideTimeout;
	        var autoHideDuration = _props4.autoHideDuration;
	        var thumbSize = _props4.thumbSize;
	        var thumbMinSize = _props4.thumbMinSize;
	        var universal = _props4.universal;
	        var style = _props4.style;
	        var children = _props4.children;

	        var props = _objectWithoutProperties(_props4, ['onScroll', 'onScrollFrame', 'onScrollStart', 'onScrollStop', 'renderView', 'renderTrackHorizontal', 'renderTrackVertical', 'renderThumbHorizontal', 'renderThumbVertical', 'autoHide', 'autoHideTimeout', 'autoHideDuration', 'thumbSize', 'thumbMinSize', 'universal', 'style', 'children']);

	        var didMountUniversal = this.state.didMountUniversal;


	        var containerStyle = _extends({}, _styles.containerStyleDefault, style);

	        var viewStyle = _extends({}, _styles.viewStyleDefault, scrollbarWidth ? { right: -scrollbarWidth, bottom: -scrollbarWidth } : { right: 0, bottom: 0 }, universal && !didMountUniversal ? _styles.viewStyleUniversalInitial : undefined);

	        var trackHorizontalStyle = _extends({}, _styles.trackHorizontalStyleDefault, autoHide ? { transition: 'opacity ' + autoHideDuration + 'ms', opacity: 0 } : undefined, !scrollbarWidth || universal && !didMountUniversal ? { display: 'none' } : undefined);

	        var trackVerticalStyle = _extends({}, _styles.trackVerticalStyleDefault, autoHide ? { transition: 'opacity ' + autoHideDuration + 'ms', opacity: 0 } : undefined, !scrollbarWidth || universal && !didMountUniversal ? { display: 'none' } : undefined);

	        return _react2["default"].createElement(
	            'div',
	            _extends({}, props, { style: containerStyle, ref: 'container' }),
	            (0, _react.cloneElement)(renderView({ style: viewStyle }), { ref: 'view' }, children),
	            (0, _react.cloneElement)(renderTrackHorizontal({ style: trackHorizontalStyle }), { ref: 'trackHorizontal' }, (0, _react.cloneElement)(renderThumbHorizontal({ style: _styles.thumbHorizontalStyleDefault }), { ref: 'thumbHorizontal' })),
	            (0, _react.cloneElement)(renderTrackVertical({ style: trackVerticalStyle }), { ref: 'trackVertical' }, (0, _react.cloneElement)(renderThumbVertical({ style: _styles.thumbVerticalStyleDefault }), { ref: 'thumbVertical' }))
	        );
	    }
	});

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var containerStyleDefault = exports.containerStyleDefault = {
	    position: 'relative',
	    overflow: 'hidden',
	    width: '100%',
	    height: '100%'
	};

	var viewStyleDefault = exports.viewStyleDefault = {
	    position: 'absolute',
	    top: 0,
	    left: 0,
	    overflow: 'scroll',
	    WebkitOverflowScrolling: 'touch'
	};

	var viewStyleUniversalInitial = exports.viewStyleUniversalInitial = {
	    right: 0,
	    bottom: 0,
	    overflow: 'hidden'
	};

	var trackHorizontalStyleDefault = exports.trackHorizontalStyleDefault = {
	    position: 'absolute',
	    height: 6
	};

	var trackVerticalStyleDefault = exports.trackVerticalStyleDefault = {
	    position: 'absolute',
	    width: 6
	};

	var thumbHorizontalStyleDefault = exports.thumbHorizontalStyleDefault = {
	    position: 'relative',
	    display: 'block',
	    height: '100%'
	};

	var thumbVerticalStyleDefault = exports.thumbVerticalStyleDefault = {
	    position: 'relative',
	    display: 'block',
	    width: '100%'
	};

	var disableSelectStyle = exports.disableSelectStyle = {
	    userSelect: 'none'
	};

	var disableSelectStyleReset = exports.disableSelectStyleReset = {
	    userSelect: ''
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = getInnerHeight;
	function getInnerHeight(el) {
	    var clientHeight = el.clientHeight;

	    var _getComputedStyle = getComputedStyle(el);

	    var paddingTop = _getComputedStyle.paddingTop;
	    var paddingBottom = _getComputedStyle.paddingBottom;

	    return clientHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
	}

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = getInnerWidth;
	function getInnerWidth(el) {
	    var clientWidth = el.clientWidth;

	    var _getComputedStyle = getComputedStyle(el);

	    var paddingLeft = _getComputedStyle.paddingLeft;
	    var paddingRight = _getComputedStyle.paddingRight;

	    return clientWidth - parseFloat(paddingLeft) - parseFloat(paddingRight);
	}

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = getScrollbarWidth;

	var _domCss = __webpack_require__(1);

	var _domCss2 = _interopRequireDefault(_domCss);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

	var scrollbarWidth = false;

	function getScrollbarWidth() {
	    if (scrollbarWidth !== false) return scrollbarWidth;
	    /* istanbul ignore else */
	    if (typeof document !== 'undefined') {
	        var div = document.createElement('div');
	        (0, _domCss2["default"])(div, {
	            width: 100,
	            height: 100,
	            position: 'absolute',
	            top: -9999,
	            overflow: 'scroll',
	            MsOverflowStyle: 'scrollbar'
	        });
	        document.body.appendChild(div);
	        scrollbarWidth = div.offsetWidth - div.clientWidth;
	        document.body.removeChild(div);
	    } else {
	        scrollbarWidth = 0;
	    }
	    return scrollbarWidth;
	}

/***/ },
/* 9 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports["default"] = returnFalse;
	function returnFalse() {
	    return false;
	}

/***/ },
/* 10 */
/***/ function(module, exports) {

	/* The following list is defined in React's core */
	var IS_UNITLESS = {
	  animationIterationCount: true,
	  boxFlex: true,
	  boxFlexGroup: true,
	  boxOrdinalGroup: true,
	  columnCount: true,
	  flex: true,
	  flexGrow: true,
	  flexPositive: true,
	  flexShrink: true,
	  flexNegative: true,
	  flexOrder: true,
	  gridRow: true,
	  gridColumn: true,
	  fontWeight: true,
	  lineClamp: true,
	  lineHeight: true,
	  opacity: true,
	  order: true,
	  orphans: true,
	  tabSize: true,
	  widows: true,
	  zIndex: true,
	  zoom: true,

	  // SVG-related properties
	  fillOpacity: true,
	  stopOpacity: true,
	  strokeDashoffset: true,
	  strokeOpacity: true,
	  strokeWidth: true
	};

	module.exports = function(name, value) {
	  if(typeof value === 'number' && !IS_UNITLESS[ name ]) {
	    return value + 'px';
	  } else {
	    return value;
	  }
	};

/***/ },
/* 11 */
/***/ function(module, exports) {

	var div = null
	var prefixes = [ 'Webkit', 'Moz', 'O', 'ms' ]

	module.exports = function prefixStyle (prop) {
	  // re-use a dummy div
	  if (!div) {
	    div = document.createElement('div')
	  }

	  var style = div.style

	  // prop exists without prefix
	  if (prop in style) {
	    return prop
	  }

	  // borderRadius -> BorderRadius
	  var titleCase = prop.charAt(0).toUpperCase() + prop.slice(1)

	  // find the vendor-prefixed prop
	  for (var i = prefixes.length; i >= 0; i--) {
	    var name = prefixes[i] + titleCase
	    // e.g. WebkitBorderRadius or webkitBorderRadius
	    if (name in style) {
	      return name
	    }
	  }

	  return false
	}


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	
	var toSpace = __webpack_require__(13);


	/**
	 * Expose `toCamelCase`.
	 */

	module.exports = toCamelCase;


	/**
	 * Convert a `string` to camel case.
	 *
	 * @param {String} string
	 * @return {String}
	 */


	function toCamelCase (string) {
	  return toSpace(string).replace(/\s(\w)/g, function (matches, letter) {
	    return letter.toUpperCase();
	  });
	}

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	
	var clean = __webpack_require__(14);


	/**
	 * Expose `toSpaceCase`.
	 */

	module.exports = toSpaceCase;


	/**
	 * Convert a `string` to space case.
	 *
	 * @param {String} string
	 * @return {String}
	 */


	function toSpaceCase (string) {
	  return clean(string).replace(/[\W_]+(.|$)/g, function (matches, match) {
	    return match ? ' ' + match : '';
	  });
	}

/***/ },
/* 14 */
/***/ function(module, exports) {

	
	/**
	 * Expose `toNoCase`.
	 */

	module.exports = toNoCase;


	/**
	 * Test whether a string is camel-case.
	 */

	var hasSpace = /\s/;
	var hasCamel = /[a-z][A-Z]/;
	var hasSeparator = /[\W_]/;


	/**
	 * Remove any starting case from a `string`, like camel or snake, but keep
	 * spaces and punctuation that may be important otherwise.
	 *
	 * @param {String} string
	 * @return {String}
	 */

	function toNoCase (string) {
	  if (hasSpace.test(string)) return string.toLowerCase();

	  if (hasSeparator.test(string)) string = unseparate(string);
	  if (hasCamel.test(string)) string = uncamelize(string);
	  return string.toLowerCase();
	}


	/**
	 * Separator splitter.
	 */

	var separatorSplitter = /[\W_]+(.|$)/g;


	/**
	 * Un-separate a `string`.
	 *
	 * @param {String} string
	 * @return {String}
	 */

	function unseparate (string) {
	  return string.replace(separatorSplitter, function (m, next) {
	    return next ? ' ' + next : '';
	  });
	}


	/**
	 * Camelcase splitter.
	 */

	var camelSplitter = /(.)([A-Z]+)/g;


	/**
	 * Un-camelcase a `string`.
	 *
	 * @param {String} string
	 * @return {String}
	 */

	function uncamelize (string) {
	  return string.replace(camelSplitter, function (m, previous, uppers) {
	    return previous + ' ' + uppers.toLowerCase().split('').join(' ');
	  });
	}

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var now = __webpack_require__(16)
	  , global = typeof window === 'undefined' ? {} : window
	  , vendors = ['moz', 'webkit']
	  , suffix = 'AnimationFrame'
	  , raf = global['request' + suffix]
	  , caf = global['cancel' + suffix] || global['cancelRequest' + suffix]

	for(var i = 0; i < vendors.length && !raf; i++) {
	  raf = global[vendors[i] + 'Request' + suffix]
	  caf = global[vendors[i] + 'Cancel' + suffix]
	      || global[vendors[i] + 'CancelRequest' + suffix]
	}

	// Some versions of FF have rAF but not cAF
	if(!raf || !caf) {
	  var last = 0
	    , id = 0
	    , queue = []
	    , frameDuration = 1000 / 60

	  raf = function(callback) {
	    if(queue.length === 0) {
	      var _now = now()
	        , next = Math.max(0, frameDuration - (_now - last))
	      last = next + _now
	      setTimeout(function() {
	        var cp = queue.slice(0)
	        // Clear queue here to prevent
	        // callbacks from appending listeners
	        // to the current frame's queue
	        queue.length = 0
	        for(var i = 0; i < cp.length; i++) {
	          if(!cp[i].cancelled) {
	            try{
	              cp[i].callback(last)
	            } catch(e) {
	              setTimeout(function() { throw e }, 0)
	            }
	          }
	        }
	      }, Math.round(next))
	    }
	    queue.push({
	      handle: ++id,
	      callback: callback,
	      cancelled: false
	    })
	    return id
	  }

	  caf = function(handle) {
	    for(var i = 0; i < queue.length; i++) {
	      if(queue[i].handle === handle) {
	        queue[i].cancelled = true
	      }
	    }
	  }
	}

	module.exports = function(fn) {
	  // Wrap in a new function to prevent
	  // `cancel` potentially being assigned
	  // to the native rAF function
	  return raf.call(global, fn)
	}
	module.exports.cancel = function() {
	  caf.apply(global, arguments)
	}


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {// Generated by CoffeeScript 1.7.1
	(function() {
	  var getNanoSeconds, hrtime, loadTime;

	  if ((typeof performance !== "undefined" && performance !== null) && performance.now) {
	    module.exports = function() {
	      return performance.now();
	    };
	  } else if ((typeof process !== "undefined" && process !== null) && process.hrtime) {
	    module.exports = function() {
	      return (getNanoSeconds() - loadTime) / 1e6;
	    };
	    hrtime = process.hrtime;
	    getNanoSeconds = function() {
	      var hr;
	      hr = hrtime();
	      return hr[0] * 1e9 + hr[1];
	    };
	    loadTime = getNanoSeconds();
	  } else if (Date.now) {
	    module.exports = function() {
	      return Date.now() - loadTime;
	    };
	    loadTime = Date.now();
	  } else {
	    module.exports = function() {
	      return new Date().getTime() - loadTime;
	    };
	    loadTime = new Date().getTime();
	  }

	}).call(this);

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(17)))

/***/ },
/* 17 */
/***/ function(module, exports) {

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }
/******/ ])
});
;