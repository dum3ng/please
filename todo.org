** DONE when save db, need to save config as well
   CLOSED: [2018-01-30 Tue 06:23]
   for record the maximum id

** DONE how to improve the save machanism?
   CLOSED: [2018-01-30 Tue 06:23]
- auto save after some intervals for changed buffers.
- when changed file view, if buffer has changed,
then the file should be saved. 
and as well, if the buffer has changed, but the file view 
does not change, then the buffer will be autosaved after
some time intervals.

*done* use the second method



** when to save the db?
Edits will change the db:
- add/remove/edit tag 
- edit/  file name
- add/delete file 

since the whole db may be large, save teh db
when every above edit occurs, may be ineffecient.

Two alternatives:
1. only save the db when app quit. or after some idle time 
 intervals.
2. create single file for every tag. And teh file only
 contains the tag ?

** DONE do search
   CLOSED: [2018-01-31 Wed 04:47]

use =ag= and =child_process= to do this.
#+BEGIN_SRC clojure
# db
{:search {:searching true
:}}

#+END_SRC
db:
{:}

** TODO improve the Ui
