(ns dev.core
  (:require [figwheel.client :as fw :include-macros true]
            [ui.core]
            [re-frame.core :refer [dispatch]]))

(fw/watch-and-reload
 :websocket-url   "ws://localhost:5000/figwheel-ws"
 :before-jsload #(dispatch [:db/save])
 :on-jsload (fn [] (print "reloaded")))
