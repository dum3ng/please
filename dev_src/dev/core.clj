(ns dev.core
  (:require [figwheel-sidecar.repl-api :as ra]
            [figwheel-sidecar.config :refer [get-project-builds]]))

(defn start []
  (ra/start-figwheel!
   {:server-port 5000
    :builds (get-project-builds)
    :builds-to-start ["frontend-dev"]}))

(defn repl
  ([]
   (repl "frontend-dev"))
  ([id]
   (ra/cljs-repl id)))
