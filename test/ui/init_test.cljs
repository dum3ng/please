(ns ui.init-test
  (:require
   [ui.utils :as u]
   [re-frame.core :refer [dispatch-sync
                          subscribe]]
   [cljs.test :as t :include-macros true]))

(t/deftest init-test
  (t/testing "test read config"
    (let [config (u/read-config)]
      (print config)
      (t/is (= 6 6))
      (t/is (= (:id config ) 6)
            )
      (t/is (not (nil? (:root config))))))


  (t/testing "test get info"
    (let [root (u/get-info)
          db (:db root)]

      (t/is (= (:id root ) 6))
      (t/is (= (count (:tags db)) 3) "should have 3 tags"))))


(t/deftest utils-test
  (t/testing "assoc fns"
    (let [m {:a 3 :b 4}
          m2 (u/assoc-if m (= 3 3)
                         :c 5)]
      (t/is (= 5 (:c m2)) "m2 should have a c with 5"))))


(t/deftest events-test

  (t/testing "check tag exists"
    (dispatch-sync [:initialize-db-prod])
    (let [db (subscribe [:db])
          tag-name "TECH"
          tag (some #(if (= tag-name (:name (last %)))(last %) ) (:tags @db))]
      (t/is  (= "TECH" (:name tag ))  "should ahve TECH tag"))    )

  ;; (t/testing "tags maniputation"
  ;;   (dispatch-sync [:initialize-db-prod])
  ;;   )
  )
